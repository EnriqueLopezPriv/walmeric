function palindromo(word){
    var loWord = word.toLowerCase().replace(/\s/g,"");
    var arrayWord = loWord.split("");
    var palindrome = arrayWord.reverse().join("");

    return loWord === palindrome;
}

console.log("PALINDROMO");
console.log(palindromo("Dabale arroz a la zorra el abad"));
console.log(palindromo("Hola mundo"));

//

function multiplosSiete(numbers){
    var multip = [];
    var i = 0;
    while(i < numbers.length){
        if(numbers[i] % 7 == 0) multip.push(numbers[i]);
        i++;
    }
    return multip;
}

const numbers = [1, 8, 7, 49, 23, 21, 12, 19, 5, 73, 63, 9, 14];
console.log("MULTIPLOS DE SIETE");
console.log(multiplosSiete(numbers));

//

function name_age(person){
    return({name: person.name, age:person.age});
}

function no_sensitive(person){
    var filtered = Object.assign({}, person); //Queremos devolverlo, no cargarnos el que tenemos
    delete filtered.employed; delete filtered.dni;
    return(filtered);
}

var persona = {
    "name": "John",
    "surname": "Doe",
    "age": "30",
    "country": "Spain",
    "city": "Madrid",
    "employed": "true",
    "dni": "00000000A",
};
console.log("OBJETO PERSONA. Solo nombre y edad");
console.log(name_age(persona));
console.log("OBJETO PERSONA. Sin DNI ni empleo");
console.log(no_sensitive(persona));

//

async function getTodos(){
    try{
        var response = await fetch('https://jsonplaceholder.typicode.com/todos/');
        var response_json = await response.json();
        console.log(response_json);
    } catch (error){
        console.log(error);
    }
}
console.log("LLAMADA A API");
getTodos();

/* RESPUESTAS TEST
Dada la siguiente declaración de objetos... -> Los 3 objetos son iguales
Teniendo en cuenta la declaración de objetos anterior... -> obj y obj3 son iguales
*/